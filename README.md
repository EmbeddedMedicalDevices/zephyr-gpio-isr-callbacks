[![pipeline status](https://gitlab.oit.duke.edu/EmbeddedMedicalDevices/zephyr-gpio-isr-callbacks/badges/main/pipeline.svg)](https://gitlab.oit.duke.edu/EmbeddedMedicalDevices/zephyr-gpio-isr-callbacks/-/commits/main)


# Zephyr: GPIO, ISR & Callbacks

Module covering GPIO, ISRs / callbacks, functional block diagrams and
devicetrees.

* HTML Slides: https://embeddedmedicaldevices.pages.oit.duke.edu/zephyr-gpio-isr-callbacks/Zephyr-GPIO-ISR-Callbacks.html
* PDF Slides: https://embeddedmedicaldevices.pages.oit.duke.edu/zephyr-gpio-isr-callbacks/Zephyr-GPIO-ISR-Callbacks.pdf

## Licensing
<a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/deed.en_US"><img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by-sa/3.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Zephyr-GPIO-ISR-Callbacks</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://github.com/mlp6/" property="cc:attributionName" rel="cc:attributionURL">Mark L. Palmeri</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/deed.en_US">Creative Commons Attribution-ShareAlike 3.0 Unported License</a>.<br />Based on a work at <a xmlns:dct="http://purl.org/dc/terms/" href="https://gitlab.oit.duke.edu/EmbeddedMedicalDevices/zephyr-gpio-isr-callbacks" rel="dct:source">https://gitlab.oit.duke.edu/EmbeddedMedicalDevices/zephyr-gpio-isr-callbacks</a>.

Copyright (c) 2022-2025 Mark Palmeri (Duke University)
